# parsing_fozzyshop

Парсинг на python акционных предложений в интернет магазине fozzyshop.ua

Для парсера использовались следующие версии пакетов:
pip install beautifulsoup4==4.8.1
pip install requests==2.22.0

После запуска формируется CSV файл по всем акционным позициям.